SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[BOOKING_STATUS_API_UPDATE_PROCESSED_REQUESTS]
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    UPDATE [tblBookingStatusRequest]
	SET
		[IsProcessed] = 1
	FROM 
		[tblBookingStatusRequest] REQ		
	INNER JOIN
		[tblBookingStatusResponse] RES
	ON
		REQ.ID = RES.requestId
	WHERE
		RES.[responseCode] LIKE '%SUCCESS'
		AND
		REQ.[IsProcessed] = 0
END
GO
