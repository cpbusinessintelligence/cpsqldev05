SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[CP_GET_COSMOS_DB_RATE_CUSTOMISED_HEADER_JSON] 

AS
BEGIN

	SET NOCOUNT ON;

	SELECT JSONString FROM
		(SELECT 
				[Id],
					(SELECT    
						[UniqueKey] AS [id]
						,[Accountcode]
						,[Name]
						,[Service]
						,[ValidFrom]
						,[FuelOverride]
						,[Fuel]/100 AS [Fuel]
						,[ChargeMethod]
						,[VolumetricDivisor]
						,[TariffId]
						,[Logwho]
						,[Logdate]
					FROM [dbo].[tblRateCustomisedHeaderStaging] WITH (NOLOCK)
						WHERE  [Id] = RelationalJSONData.Id
					FOR JSON PATH, INCLUDE_NULL_VALUES, WITHOUT_ARRAY_WRAPPER)JSONString
			FROM [tblRateCustomisedHeaderStaging]  AS RelationalJSONData  WITH (NOLOCK)) AS
	JSONOnly
END
GO
