SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[CP_GET_COSMOS_DB_CUSTOMISED_RATE_CARD_DETAIL_JSON] 
(
	@Lower INT,
	@Upper INT
)

AS
BEGIN

	SET NOCOUNT ON;

	SELECT JSONString FROM
		(SELECT 
				[Id],
					(SELECT    
						[UniqueKey] AS [id]
						,[Account]
						,[Service]
						,[EffectiveDate]
						,[OriginZone]
						,[DestinationZone]
						,[MinimumCharge]
						,[BasicCharge]
						,[FuelOverride]
						,[FuelPercentage]/100 AS [FuelPercentage]
						,[Rounding]
						,[ChargePerKilo]
					FROM [dbo].[tblCustomisedRateCardDetailStaging] WITH (NOLOCK)
						WHERE  [Id] = RelationalJSONData.Id
					FOR JSON PATH, INCLUDE_NULL_VALUES, WITHOUT_ARRAY_WRAPPER)JSONString
			FROM [tblCustomisedRateCardDetailStaging]  AS RelationalJSONData  WITH (NOLOCK) 
			WHERE  [Id] BETWEEN @Lower AND @Upper) AS
	JSONOnly
END
GO
