SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblBookingStatusRequest] (
		[Id]                      [int] IDENTITY(1, 1) NOT NULL,
		[uniqueID]                [varchar](30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[AccountNumber]           [varchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[Branch]                  [varchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[BookingType]             [varchar](10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[ItemNumber]              [varchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[LastStatus]              [varchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[LastUpdatedDateTime]     [varchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[DriverNumber]            [varchar](10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[Timestamp]               [datetime] NOT NULL,
		[IsProcessed]             [bit] NOT NULL,
		CONSTRAINT [PK__tblBooki__3214EC0720859FB7]
		PRIMARY KEY
		CLUSTERED
		([Id])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblBookingStatusRequest]
	ADD
	CONSTRAINT [DF__tblBookin__IsPro__73BA3083]
	DEFAULT ((0)) FOR [IsProcessed]
GO
ALTER TABLE [dbo].[tblBookingStatusRequest]
	ADD
	CONSTRAINT [DF__tblBookin__Times__74AE54BC]
	DEFAULT (getdate()) FOR [Timestamp]
GO
ALTER TABLE [dbo].[tblBookingStatusRequest] SET (LOCK_ESCALATION = TABLE)
GO
