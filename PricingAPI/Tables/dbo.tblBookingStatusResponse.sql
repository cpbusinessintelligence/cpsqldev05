SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblBookingStatusResponse] (
		[id]               [int] IDENTITY(1, 1) NOT NULL,
		[requestId]        [int] NOT NULL,
		[responseCode]     [varchar](30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[statusCode]       [varchar](10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[dataId]           [varchar](30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[timestamp]        [datetime] NOT NULL,
		CONSTRAINT [PK__tblBooki__3213E83F07C231E1]
		PRIMARY KEY
		CLUSTERED
		([id])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblBookingStatusResponse]
	ADD
	CONSTRAINT [DF__tblBookin__times__778AC167]
	DEFAULT (getdate()) FOR [timestamp]
GO
ALTER TABLE [dbo].[tblBookingStatusResponse] SET (LOCK_ESCALATION = TABLE)
GO
