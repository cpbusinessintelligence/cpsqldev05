SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblRateCustomisedHeaderStaging] (
		[Id]                    [int] IDENTITY(1, 1) NOT NULL,
		[UniqueKey]             [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[Accountcode]           [varchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[Name]                  [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[Service]               [varchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[ValidFrom]             [date] NULL,
		[FuelOverride]          [varchar](5) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[Fuel]                  [float] NULL,
		[ChargeMethod]          [varchar](5) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[VolumetricDivisor]     [int] NULL,
		[TariffId]              [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[Logwho]                [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[Logdate]               [date] NULL,
		CONSTRAINT [UQ__tblRateC__2DE46E937EAC160A]
		UNIQUE
		NONCLUSTERED
		([UniqueKey])
		ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblRateCustomisedHeaderStaging] SET (LOCK_ESCALATION = TABLE)
GO
