SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblCustomisedRateCardDetailStaging] (
		[Id]                  [int] IDENTITY(1, 1) NOT NULL,
		[UniqueKey]           [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[Account]             [varchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[Service]             [varchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[EffectiveDate]       [date] NULL,
		[OriginZone]          [varchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[DestinationZone]     [varchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[MinimumCharge]       [float] NULL,
		[BasicCharge]         [float] NULL,
		[FuelOverride]        [varchar](5) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[FuelPercentage]      [float] NULL,
		[Rounding]            [float] NULL,
		[ChargePerKilo]       [float] NULL,
		CONSTRAINT [UQ__tblCusto__2DE46E93D6E54DE6]
		UNIQUE
		NONCLUSTERED
		([UniqueKey])
		ON [PRIMARY]
) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [idx_nc_tblCustomisedRateCardDetailStaging]
	ON [dbo].[tblCustomisedRateCardDetailStaging] ([Id])
	INCLUDE ([UniqueKey], [Account], [Service], [EffectiveDate], [OriginZone], [DestinationZone], [MinimumCharge], [BasicCharge], [FuelOverride], [FuelPercentage], [Rounding], [ChargePerKilo])
	ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblCustomisedRateCardDetailStaging] SET (LOCK_ESCALATION = TABLE)
GO
