SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_ErrorLog] (
		[ErrorID]                     [int] IDENTITY(1, 1) NOT NULL,
		[ErrorCode]                   [nvarchar](255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[ErrorColumn]                 [int] NULL,
		[ErrorDescription]            [varchar](8000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[ErrorData]                   [varchar](8000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[IssuingPackage]              [nvarchar](255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[Source]                      [nvarchar](255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[SourceUniqueColumnValue]     [nvarchar](255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[Destination]                 [nvarchar](255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[Timestamp]                   [datetime] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbl_ErrorLog] SET (LOCK_ESCALATION = TABLE)
GO
