SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_AgentConfiguration] (
		[AgentID]                      [int] IDENTITY(1, 1) NOT NULL,
		[AgentName]                    [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[AgentCode]                    [varchar](10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[FileType]                     [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[FileTypeExtension]            [varchar](10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[HeaderRowsCount]              [int] NULL,
		[ColumnDelimiter]              [varchar](5) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[RowPrefix]                    [varchar](5) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[FileNameCheck]                [bit] NULL,
		[FileNameCheckString]          [varchar](10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[FileNameCheckStringIndex]     [int] NULL,
		[FTPHostName]                  [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[FTPUserName]                  [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[FTPPassword]                  [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[FTPRemotePath]                [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[FTPSshHostKeyFingerprint]     [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[IsActive]                     [bit] NULL,
		[CreatedDatetime]              [datetime] NULL,
		[UpdatedDatetime]              [datetime] NULL,
		CONSTRAINT [PK_tbl_AgentConfiguration]
		PRIMARY KEY
		CLUSTERED
		([AgentID])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbl_AgentConfiguration] SET (LOCK_ESCALATION = TABLE)
GO
