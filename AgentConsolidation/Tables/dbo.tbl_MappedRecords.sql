SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_MappedRecords] (
		[MappedRecordID]        [int] IDENTITY(1, 1) NOT NULL,
		[AgentID]               [int] NOT NULL,
		[StagingRecordID]       [int] NULL,
		[TrackingNumber]        [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[ConsignmentNumber]     [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[StatusDateTime]        [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[ScanEvent]             [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[Contractor]            [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[ExceptionReason]       [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[IsProcessed]           [bit] NULL,
		[CreatedDatetime]       [datetime] NULL,
		[UpdatedDatetime]       [datetime] NULL,
		CONSTRAINT [PK_tbl_MappedRecords]
		PRIMARY KEY
		CLUSTERED
		([MappedRecordID])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbl_MappedRecords] SET (LOCK_ESCALATION = TABLE)
GO
