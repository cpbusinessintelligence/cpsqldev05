SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_ColumnMapping] (
		[RowID]           [int] IDENTITY(1, 1) NOT NULL,
		[AgentID]         [int] NULL,
		[ColumnName]      [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[ColumnKey]       [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[ColumnValue]     [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		CONSTRAINT [PK_tbl_ColumnMapping]
		PRIMARY KEY
		CLUSTERED
		([RowID])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbl_ColumnMapping] SET (LOCK_ESCALATION = TABLE)
GO
