SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_StagingRecords] (
		[StagingRecordID]     [int] IDENTITY(1, 1) NOT NULL,
		[AgentID]             [int] NULL,
		[RowData]             [varchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[FileName]            [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[IsMapped]            [bit] NULL,
		[CreatedDatetime]     [datetime] NULL,
		[UpdatedDatetime]     [datetime] NULL,
		CONSTRAINT [PK_tbl_StagingRecords]
		PRIMARY KEY
		CLUSTERED
		([StagingRecordID])
	ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbl_StagingRecords] SET (LOCK_ESCALATION = TABLE)
GO
