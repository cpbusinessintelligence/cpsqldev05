SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING OFF
GO
CREATE TABLE [dbo].[tbl_MappingTemplate_DelimitedFile] (
		[TemplateID]                       [int] IDENTITY(1, 1) NOT NULL,
		[AgentID]                          [int] NULL,
		[TrackingNumberIndex]              [int] NULL,
		[ConsignmentNumberIndex]           [int] NULL,
		[StatusDateIndex]                  [int] NULL,
		[StatusTimeIndex]                  [int] NULL,
		[ScanEventIndex]                   [int] NULL,
		[ScanEventMappingExists]           [bit] NOT NULL,
		[ExceptionReasonIndex]             [int] NULL,
		[ExceptionReasonMappingExists]     [bit] NOT NULL,
		[CreatedDatetime]                  [datetime] NULL,
		[UpdatedDatetime]                  [datetime] NULL,
		CONSTRAINT [PK_tbl_MappingTemplate_DelimitedFile]
		PRIMARY KEY
		CLUSTERED
		([TemplateID])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbl_MappingTemplate_DelimitedFile] SET (LOCK_ESCALATION = TABLE)
GO
