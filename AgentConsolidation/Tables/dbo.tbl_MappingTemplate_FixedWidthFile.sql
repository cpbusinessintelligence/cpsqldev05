SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING OFF
GO
CREATE TABLE [dbo].[tbl_MappingTemplate_FixedWidthFile] (
		[TemplateID]                       [int] IDENTITY(1, 1) NOT NULL,
		[AgentID]                          [int] NULL,
		[TrackingNumberStartIndex]         [int] NULL,
		[TrackingNumberLength]             [int] NULL,
		[ConsignmentNumberStartIndex]      [int] NULL,
		[ConsignmentNumberLength]          [int] NULL,
		[StatusDateTimeStartIndex]         [int] NULL,
		[StatusDateTimeLength]             [int] NULL,
		[ScanEventStartIndex]              [int] NULL,
		[ScanEventLength]                  [int] NULL,
		[ScanEventMappingExists]           [bit] NOT NULL,
		[ExceptionReasonStartIndex]        [int] NULL,
		[ExceptionReasonLength]            [int] NULL,
		[ExceptionReasonMappingExists]     [bit] NOT NULL,
		[CreatedDatetime]                  [datetime] NULL,
		[UpdatedDatetime]                  [datetime] NULL,
		CONSTRAINT [PK_tbl_MappingTemplate_FixedWidthFile]
		PRIMARY KEY
		CLUSTERED
		([TemplateID])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbl_MappingTemplate_FixedWidthFile] SET (LOCK_ESCALATION = TABLE)
GO
