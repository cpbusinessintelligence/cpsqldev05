SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[CP_GET_COSMOS_DB_RATE_CARD_HEADER_JSON] 

AS
BEGIN

	SET NOCOUNT ON;	

	SELECT JSONString FROM
		(SELECT 
				[Id],
					(SELECT    
						[TariffId] AS [id]
						,[Service]
						,[ValidFrom]
						,[FuelOverride]
						,[Fuel]/100 AS [Fuel]
						,[ChargeMethod]
						,[VolumetricDivisor]
						,[TariffId]
						,[Logwho]
						,[Logdate]
					FROM [dbo].[tblRateCardHeaderStaging] WITH (NOLOCK)
						WHERE  [Id] = RelationalJSONData.[Id]
					FOR JSON PATH, INCLUDE_NULL_VALUES, WITHOUT_ARRAY_WRAPPER)JSONString
			FROM [tblRateCardHeaderStaging]  AS RelationalJSONData  WITH (NOLOCK)) AS
	JSONOnly
END
GO
