SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[CP_GET_SUBURB_LOOKUP_JSON] 

AS
BEGIN

	SET NOCOUNT ON;

	SELECT JSONString FROM
		(SELECT 
				[PostCodeID],
					(SELECT CAST([PostCodeID] AS varchar) AS [PostCodeID]
					  ,[PostCode]
					  ,[Suburb]
					  ,[State]
					  ,[PrizeZone]
					  ,IIF([PrizeZoneName] = NULL OR [PrizeZoneName]='','NULL',[PrizeZoneName]) AS [PrizeZoneName]
					  ,[ETAZone]
					  ,IIF([ETAZoneName] = NULL OR [ETAZoneName]='','NULL',[ETAZoneName]) AS [ETAZoneName]
					  ,ISNULL([RedemptionZone], 'NULL') AS [RedemptionZone]
					  ,IIF([RedemptionZoneName] = NULL OR [RedemptionZoneName]='','NULL',[RedemptionZoneName]) AS [RedemptionZoneName]
					  ,[DepotCode]
					  ,[SortCode]
					  ,[IsPickup]
					  ,[IsDelivery]
					  ,[PostcodeSuburb] AS [PostCodeSuburb]
					  ,[IsActive]
					  ,CAST(CAST([PostCode] AS INT)AS VARCHAR) AS [PostCodeOld]
					FROM [dbo].[SuburbLookup] WITH (NOLOCK)
						WHERE  [PostCodeID] = RelationalJSONData.[PostCodeID]
					FOR JSON PATH, INCLUDE_NULL_VALUES, WITHOUT_ARRAY_WRAPPER)JSONString
			FROM [SuburbLookup]  AS RelationalJSONData  WITH (NOLOCK) 
			) AS
	JSONOnly
END
GO
