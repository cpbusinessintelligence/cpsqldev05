SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[SuburbLookup] (
		[PostCodeID]             [int] NOT NULL,
		[PostCode]               [varchar](10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[Suburb]                 [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[State]                  [varchar](30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[PrizeZone]              [varchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[PrizeZoneName]          [varchar](1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[ETAZone]                [varchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[ETAZoneName]            [varchar](1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[RedemptionZone]         [varchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[RedemptionZoneName]     [varchar](1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[DepotCode]              [varchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[SortCode]               [varchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[IsPickup]               [varchar](5) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[IsDelivery]             [varchar](5) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[PostcodeSuburb]         [varchar](60) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[IsActive]               [varchar](1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[SuburbLookup] SET (LOCK_ESCALATION = TABLE)
GO
