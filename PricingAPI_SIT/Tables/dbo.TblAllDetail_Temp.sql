SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TblAllDetail_Temp] (
		[Id]                  [int] IDENTITY(1, 1) NOT NULL,
		[UniqueKey]           [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[Account]             [varchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[Service]             [varchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[EffectiveDate]       [date] NULL,
		[OriginZone]          [varchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[DestinationZone]     [varchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[MinimumCharge]       [float] NULL,
		[BasicCharge]         [float] NULL,
		[FuelOverride]        [varchar](5) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[FuelPercentage]      [float] NULL,
		[Rounding]            [float] NULL,
		[ChargePerKilo]       [float] NULL,
		CONSTRAINT [UQ__TblAllDe__2DE46E93769C3293]
		UNIQUE
		NONCLUSTERED
		([UniqueKey])
		ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[TblAllDetail_Temp] SET (LOCK_ESCALATION = TABLE)
GO
