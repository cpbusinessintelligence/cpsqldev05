SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblRateCardHeaderStaging] (
		[Id]                    [int] IDENTITY(1, 1) NOT NULL,
		[Service]               [varchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[ValidFrom]             [date] NULL,
		[FuelOverride]          [varchar](5) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[Fuel]                  [float] NULL,
		[ChargeMethod]          [varchar](5) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[VolumetricDivisor]     [int] NULL,
		[TariffId]              [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[Logwho]                [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[Logdate]               [date] NULL
) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [idx_nc_tblRateCardHeaderStaging]
	ON [dbo].[tblRateCardHeaderStaging] ([Id])
	INCLUDE ([Service], [ValidFrom], [FuelOverride], [Fuel], [ChargeMethod], [VolumetricDivisor], [TariffId], [Logwho], [Logdate])
	ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblRateCardHeaderStaging] SET (LOCK_ESCALATION = TABLE)
GO
