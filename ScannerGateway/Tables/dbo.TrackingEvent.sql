SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TrackingEvent] (
		[Id]                    [uniqueidentifier] NOT NULL ROWGUIDCOL,
		[EventTypeId]           [uniqueidentifier] NOT NULL,
		[EventDateTime]         [datetime] NOT NULL,
		[Description]           [varchar](200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[LabelId]               [uniqueidentifier] NULL,
		[AgentId]               [uniqueidentifier] NULL,
		[DriverId]              [uniqueidentifier] NULL,
		[RunId]                 [uniqueidentifier] NULL,
		[EventLocation]         [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[ExceptionReason]       [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[AdditionalText1]       [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[AdditionalText2]       [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[NumberOfItems]         [smallint] NULL,
		[SourceId]              [uniqueidentifier] NULL,
		[SourceReference]       [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[CosmosSignatureId]     [int] NULL,
		[CosmosBranchId]        [int] NULL,
		[IsDeleted]             [bit] NOT NULL,
		[CreatedDate]           [datetime] NOT NULL,
		[LastModifiedDate]      [datetime] NOT NULL,
		CONSTRAINT [PK_TrackingEvent]
		PRIMARY KEY
		NONCLUSTERED
		([Id])
	ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[TrackingEvent]
	ADD
	CONSTRAINT [DF_TrackingEvent_Id]
	DEFAULT (newid()) FOR [Id]
GO
ALTER TABLE [dbo].[TrackingEvent]
	ADD
	CONSTRAINT [DF_TrackingEvent_NumberOfItems]
	DEFAULT ((1)) FOR [NumberOfItems]
GO
ALTER TABLE [dbo].[TrackingEvent]
	ADD
	CONSTRAINT [DF_TrackingEvent_IsDeleted]
	DEFAULT ((0)) FOR [IsDeleted]
GO
ALTER TABLE [dbo].[TrackingEvent]
	ADD
	CONSTRAINT [DF_TrackingEvent_CreatedDate]
	DEFAULT (getdate()) FOR [CreatedDate]
GO
ALTER TABLE [dbo].[TrackingEvent]
	ADD
	CONSTRAINT [DF_TrackingEvent_LastModifiedDate]
	DEFAULT (getdate()) FOR [LastModifiedDate]
GO
ALTER TABLE [dbo].[TrackingEvent] SET (LOCK_ESCALATION = TABLE)
GO
