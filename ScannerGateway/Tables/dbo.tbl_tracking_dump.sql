SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_tracking_dump] (
		[id]               [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[type]             [nvarchar](500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[url]              [nvarchar](500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[body]             [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[created_at]       [nvarchar](1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[succeeded_at]     [nvarchar](1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbl_tracking_dump] SET (LOCK_ESCALATION = TABLE)
GO
