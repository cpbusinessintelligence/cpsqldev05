SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[BEIntegrationStaging] (
		[ScanTypeId]        [int] NOT NULL,
		[Barcode]           [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[UserId]            [int] NOT NULL,
		[ScanDate]          [varchar](40) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[LocationType]      [int] NOT NULL,
		[BranchId]          [int] NULL,
		[MethodId]          [int] NOT NULL,
		[ApplicationId]     [int] NOT NULL,
		[AreaId]            [int] NOT NULL,
		[isprocessed]       [int] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[BEIntegrationStaging] SET (LOCK_ESCALATION = TABLE)
GO
