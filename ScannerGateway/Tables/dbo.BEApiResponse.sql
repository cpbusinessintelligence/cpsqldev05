SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[BEApiResponse] (
		[Sno]              [int] IDENTITY(1, 1) NOT NULL,
		[LabelNo]          [varchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[ResponseCode]     [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[createdDate]      [datetime] NULL,
		[IsProcessed]      [bit] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[BEApiResponse]
	ADD
	CONSTRAINT [DefDatetime]
	DEFAULT (getdate()) FOR [createdDate]
GO
ALTER TABLE [dbo].[BEApiResponse]
	ADD
	CONSTRAINT [DF__BEApiResp__IsPro__4222D4EF]
	DEFAULT ((0)) FOR [IsProcessed]
GO
ALTER TABLE [dbo].[BEApiResponse] SET (LOCK_ESCALATION = TABLE)
GO
