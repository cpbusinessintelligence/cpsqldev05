SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblAgentTrackingStatusMapping] (
		[Status_Code]      [varchar](5) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[Status_Desc]      [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[created_time]     [datetime] NULL,
		[Agent_name]       [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblAgentTrackingStatusMapping]
	ADD
	CONSTRAINT [DF__tblScanEv__creat__628FA481]
	DEFAULT (getdate()) FOR [created_time]
GO
ALTER TABLE [dbo].[tblAgentTrackingStatusMapping] SET (LOCK_ESCALATION = TABLE)
GO
