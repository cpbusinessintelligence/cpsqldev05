SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblDelimitedAgentsStagingLoad] (
		[Agent_Name]         [varchar](12) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[File_Name]          [varchar](24) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[ItemNum]            [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[Connum]             [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[ReceiverName]       [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[StatusDate]         [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[StatusTime]         [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[Status]             [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[Exception_Code]     [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblDelimitedAgentsStagingLoad] SET (LOCK_ESCALATION = TABLE)
GO
