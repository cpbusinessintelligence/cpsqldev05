SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblAgentsTrackingDetail] (
		[tracking_number]      [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[consignment]          [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[status_datetime]      [datetime] NULL,
		[scan_event]           [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[contractor]           [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[Exception_Reason]     [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblAgentsTrackingDetail] SET (LOCK_ESCALATION = TABLE)
GO
