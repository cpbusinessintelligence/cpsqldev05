SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblAgentTrackingConfiguration] (
		[Agent_Name]                [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[Source_File_Format]        [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[Folder_Path]               [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[Tracking_StartStr]         [int] NULL,
		[Tracking_EndStr]           [int] NULL,
		[Consignment_StartStr]      [int] NULL,
		[Consignment_EndStr]        [int] NULL,
		[StatusDate_StartStr]       [int] NULL,
		[StatusDAte_EndStr]         [int] NULL,
		[ScanEvent_StartStr]        [int] NULL,
		[ScanEvent_EndStr]          [int] NULL,
		[Contractor]                [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[Exception_StartStr]        [int] NULL,
		[Exception_EndStr]          [int] NULL,
		[Header_Row]                [int] NULL,
		[Fixed_Width_Ind]           [varchar](1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[Trailer_Char]              [varchar](1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[Status_Code_Ind]           [varchar](1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[Status_DateConcat_Ind]     [varchar](1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[Exception_Code_Ind]        [varchar](1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[Scandate_Format]           [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[Scandate_Padding_Ind]      [varchar](1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[Year_StartNo]              [int] NULL,
		[Year_EndNo]                [int] NULL,
		[Month_StartNo]             [int] NULL,
		[Month_EndNo]               [int] NULL,
		[Day_StartNo]               [int] NULL,
		[Day_EndNo]                 [int] NULL,
		[Hour_StartNo]              [int] NULL,
		[Hour_EndNo]                [int] NULL,
		[Minute_StartNo]            [int] NULL,
		[Minute_EndNo]              [int] NULL,
		[Scandate_PMAM_Ind]         [varchar](1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblAgentTrackingConfiguration] SET (LOCK_ESCALATION = TABLE)
GO
