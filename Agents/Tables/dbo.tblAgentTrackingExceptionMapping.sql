SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblAgentTrackingExceptionMapping] (
		[Agent_Name]           [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[Exception_Code]       [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[Exception_Reason]     [varchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[Created_Time]         [datetime] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblAgentTrackingExceptionMapping]
	ADD
	CONSTRAINT [DF__tblAgentE__Creat__1BC821DD]
	DEFAULT (getdate()) FOR [Created_Time]
GO
ALTER TABLE [dbo].[tblAgentTrackingExceptionMapping] SET (LOCK_ESCALATION = TABLE)
GO
