SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblFixedWidthAgentsStagingLoad] (
		[Agent_Name]       [varchar](52) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[File_Name]        [nvarchar](52) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[File_Message]     [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblFixedWidthAgentsStagingLoad] SET (LOCK_ESCALATION = TABLE)
GO
