SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO








CREATE PROCEDURE [dbo].[Agent_InputFile_To_StagingTableLoad]
(
--Input parameter ,  Key to execute Statement 
@AgentName varchar(50),
@Filename nvarchar(50)
)

as
BEGIN
SET NOCOUNT ON; 
--SET DATEFORMAT YYYYMMDD;

DECLARE @FolderFilename nvarchar(100);
DECLARE @FormatFilename nvarchar(100);
DECLARE @FolderPath varchar(50);

select distinct @FolderPath = [folder_path] from [dbo].[tblAgentTrackingConfiguration]
where [agent_name] = ''+@AgentName+'';

SET @FolderFilename = @FolderPath+@Filename;
set @FormatFilename = @FolderPath+@AgentName+'.fmt';

DECLARE @FixedWidth varchar(1);
DECLARE @TrailerChar varchar(1);
DECLARE @FirstRow varchar(1);
DECLARE @ExceptionCodeInd varchar(1);

select distinct @FixedWidth = [Fixed_Width_Ind] from [dbo].[tblAgentTrackingConfiguration]
where [agent_name] = ''+@AgentName+'';

select distinct @TrailerChar = [Trailer_Char] from [dbo].[tblAgentTrackingConfiguration]
where [agent_name] = ''+@AgentName+'';

select distinct @ExceptionCodeInd = [Exception_Code_Ind] from [dbo].[tblAgentTrackingConfiguration]
where [agent_name] = ''+@AgentName+'';

select distinct @FirstRow = convert(varchar,[Header_row]+1) from [dbo].[tblAgentTrackingConfiguration]
where [agent_name] = ''+@AgentName+'';


DECLARE @textfile_sql NVARCHAR(4000) = 'BULK INSERT dbo.tblAgentTextFileTemp FROM ''' + @FolderFilename + ''' WITH ( firstrow = '+@FirstRow+', ROWTERMINATOR =''\n'' )';

DECLARE @delimitedfile_sql nvarchar(4000) = 'select '''+@AgentName+''' as Agent_Name, 
					'''+@Filename+''' as File_Name, a.* into tblDelimitedAgentsStagingLoad from (Select * from OPENROWSET(BULK '''+ @FolderFilename+ ''', FORMATFILE = '''+@FormatFilename +''', FIRSTROW= '+@FirstRow+') AS A)a';

IF (@FixedWidth = 'Y' )
begin
					--drop table if exists tblFixedWidthAgentsStagingLoad;
					create table tblAgentTextFileTemp
					(
					File_message nvarchar(max)
					);
					exec (@textfile_sql);
					insert into tblFixedWidthAgentsStagingLoad 
					select 
					''+@AgentName+'' as Agent_Name, 
					''+@Filename+'' as File_Name, 
					File_Message from tblAgentTextFileTemp
					where (file_message not like ''+@TrailerChar+'%'+'' or @TrailerChar is null);							
 end

 IF (@FixedWidth = 'N' and @ExceptionCodeInd = 'Y')
 Begin
 drop table if exists tblDelimitedAgentsStagingLoad;
 exec (@delimitedfile_sql);
 
 end

 IF (@FixedWidth = 'N' and @ExceptionCodeInd = 'N')
 Begin
 drop table if exists tblDelimitedAgentsStagingLoad;
 exec (@delimitedfile_sql);
 alter table tblDelimitedAgentsStagingLoad add Exception_Code varchar(100);
 end

DROP TABLE if exists tblAgentTextFileTemp;
 
end
GO
