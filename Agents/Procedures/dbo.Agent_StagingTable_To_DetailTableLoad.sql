SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO



CREATE procedure [dbo].[Agent_StagingTable_To_DetailTableLoad]
(
@AgentName VARCHAR(100)
)
AS

BEGIN
SET NOCOUNT ON;

DECLARE @FixedWidth VARCHAR(1);
DECLARE @ScanCodeInd VARCHAR(1);
DECLARE @StatusDateFormatInd VARCHAR(1);
DECLARE @ExceptionCodeInd VARCHAR(1);
DECLARE @ScandatePadInd VARCHAR(1);
DECLARE @ScandatePMAMInd VARCHAR(1);

select distinct @ScandatePMAMInd = [Scandate_PMAM_Ind] from [dbo].[tblAgentTrackingConfiguration]
where [agent_name] = ''+@AgentName+'';

select distinct @ScandatePadInd = [Scandate_Padding_Ind] from [dbo].[tblAgentTrackingConfiguration]
where [agent_name] = ''+@AgentName+'';

select distinct @ExceptionCodeInd = [Exception_Code_Ind] from [dbo].[tblAgentTrackingConfiguration]
where [agent_name] = ''+@AgentName+'';

select distinct @FixedWidth = [Fixed_Width_Ind] from [dbo].[tblAgentTrackingConfiguration]
where [agent_name] = ''+@AgentName+'';

select distinct @ScanCodeInd = [Status_Code_Ind] from [dbo].[tblAgentTrackingConfiguration]
where [agent_name] = ''+@AgentName+'';

select distinct @StatusDateFormatInd = [Status_DateConcat_Ind] from [dbo].[tblAgentTrackingConfiguration]
where [agent_name] = ''+@AgentName+'';

if (@FixedWidth = 'Y')
begin
		insert into [dbo].[tblAgentsTrackingDetail]
		select tracking_number, consignment, ScanEvent_Datetime, Scan_Event, Contractor, Exception_reason
		from
		(
		select substring(a.[File_Message],b.Tracking_StartStr, b.Tracking_EndStr) as [tracking_number],
		substring(a.[File_Message],b.Consignment_StartStr, b.Consignment_EndStr) as [consignment],

		convert(datetime, 
		substring(substring(a.[File_Message],b.StatusDate_StartStr, b.StatusDAte_EndStr), b.year_startno, b.year_endno) +'-'+
		substring(substring(a.[File_Message],b.StatusDate_StartStr, b.StatusDAte_EndStr), b.month_startno, b.month_endno) +'-'+
		substring(substring(a.[File_Message],b.StatusDate_StartStr, b.StatusDAte_EndStr), b.day_startno, b.day_endno) +' '+
		substring(substring(a.[File_Message],b.StatusDate_StartStr, b.StatusDAte_EndStr), b.hour_startno, b.hour_endno) +':'+
		substring(substring(a.[File_Message],b.StatusDate_StartStr, b.StatusDAte_EndStr), b.minute_startno, b.minute_endno))
		as ScanEvent_Datetime,

		case when c.[Status_Desc] is null then substring(a.[File_Message], b.ScanEvent_StartStr, b.ScanEvent_EndStr) 
		when b.[Status_Code_Ind] = 'Y' then c.[Status_Desc] 
		else substring(a.[File_Message], b.ScanEvent_StartStr, b.ScanEvent_EndStr) end as Scan_Event,
		b.[contractor] as Contractor,
		b.scandate_format as scandate_Format,
		case  when d.[Exception_Reason] is null then '' when b.[Exception_Code_Ind] = 'Y' then d.[Exception_Reason] else '' end as Exception_reason
		from [dbo].[tblFixedWidthAgentsStagingLoad] a
		left join  [dbo].[tblAgentTrackingConfiguration] b
		on a.Agent_Name = b.Agent_name
		left join [dbo].[tblAgentTrackingStatusMapping] c
		on ltrim(rtrim(replace(replace(substring(a.[File_Message], b.ScanEvent_StartStr, b.ScanEvent_EndStr), char(10), ''), char(13), ''))) = c.[Status_Code]
		and a.Agent_Name = c.Agent_name
		left join [dbo].[tblAgentTrackingExceptionMapping] d 
		on ltrim(rtrim(replace(replace(substring(a.[File_Message], b.[Exception_StartStr], b.[Exception_EndStr]), char(10), ''), char(13), ''))) = d.[Exception_Code]
		and a.Agent_Name = d.Agent_name
		) as scan_fixed
end

if (@FixedWidth = 'N' and @StatusDateFormatInd = 'N')
begin 
		insert into [dbo].[tblAgentsTrackingDetail]
		select a.Connum as [tracking_number],
		a.Connum as [consignment],

		convert(datetime, substring(a.StatusDate, b.year_startno, b.year_endno)+'-'+
		substring(a.StatusDate, b.month_startno, b.month_endno)+'-'+ 
		substring(a.StatusDate, b.day_startno, b.day_endno)+' '+
		substring(a.StatusDate, b.hour_startno, b.hour_endno)+':'+
		substring(a.StatusDate, b.minute_startno, b.minute_endno))
		as ScanEvent_Datetime,

		case when c.[Status_Desc] is null then a.Status when b.[Status_Code_Ind] = 'Y' then c.[Status_Desc] else a.Status end as Scan_Event,
		b.[contractor] as Contractor,
		
		case  when  a.[Exception_Code] is null then '' when a.[Exception_Code] is not null then d.[Exception_Reason] else '' end as Exception_reason

		from [dbo].[tblDelimitedAgentsStagingLoad] a
		left join  [dbo].[tblAgentTrackingConfiguration] b
		on a.Agent_Name = b.Agent_name
		left join [dbo].[tblAgentTrackingStatusMapping] c
		on ltrim(rtrim(replace(replace(a.[Status], char(10), ''), char(13), ''))) = rtrim(c.[Status_Code])
		and a.Agent_Name = c.Agent_name
		left join [dbo].[tblAgentTrackingExceptionMapping] d
		on ltrim(rtrim(replace(replace(a.[Exception_Code], char(10), ''), char(13), ''))) = d.[Exception_Code]
		and a.Agent_Name = d.Agent_name
end		

if(@FixedWidth = 'N' and @StatusDateFormatInd = 'Y')
begin 
		insert into [dbo].[tblAgentsTrackingDetail]
		select a.Connum as [tracking_number],
		a.Connum as [consignment],
		convert(datetime, substring(a.StatusDate+' '+a.StatusTime, b.year_startno, b.year_endno)+'-'+
		substring(a.StatusDate+' '+a.StatusTime,  b.month_startno, b.month_endno)+'-'+ 
		substring(a.StatusDate+' '+a.StatusTime, b.day_startno, b.day_endno)+' '+
		substring(a.StatusDate+' '+a.StatusTime, b.hour_startno, b.hour_endno)+':'+
		substring(a.StatusDate+' '+a.StatusTime, b.minute_startno, b.minute_endno))
		as ScanEvent_Datetime,
		case when c.[Status_Desc] is null then a.Status when b.[Status_Code_Ind] = 'Y' then c.[Status_Desc] else a.Status end as Scan_Event,
		b.[contractor] as Contractor,
		
		case  when  a.[Exception_Code] is null then '' when a.[Exception_Code] is not null then d.[Exception_Reason] else '' end as Exception_reason

		from [dbo].[tblDelimitedAgentsStagingLoad] a
		left join  [dbo].[tblAgentTrackingConfiguration] b
		on a.Agent_Name = b.Agent_name
		left join [dbo].[tblAgentTrackingStatusMapping] c
		on ltrim(rtrim(replace(replace(a.[Status], char(10), ''), char(13), ''))) = c.[Status_Code]
		and a.Agent_Name = c.Agent_name
		left join [dbo].[tblAgentTrackingExceptionMapping] d
		on ltrim(rtrim(replace(replace(a.[Exception_Code], char(10), ''), char(13), ''))) = d.[Exception_Code]
		and a.Agent_Name = d.Agent_name
end
 
end 

GO
